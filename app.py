from flask import Flask, jsonify, request, make_response
from flask_restful import Api, Resource
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps

import json
import jwt
import datetime
import collections
import ast

mysql = MySQL()

app = Flask(__name__)
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'assets_db'
app.config['SECRET_KEY'] = 'eyJ1c2VybmFtZSI6ImRhdmUiLCJleHAiOjE1MzUwMDgzMzl9'

api = Api(app)
mysql.init_app(app)

def resultToJson(rv, curDesc):
    row_headers = [x[0] for x in curDesc]
    json_data=[]

    # for result in rv:
    #     json_data.append(dict(zip(row_headers,result)))

    for result in rv:
        single_data = {}
        ctr = 0

        for header in row_headers:
            new_result = result[ctr].strftime('%Y-%m-%d-') if isinstance(result[ctr], datetime.date) else result[ctr]
            single_data[header] = new_result if new_result else ""
            ctr += 1
            
        json_data.append(single_data)
    
    return ast.literal_eval(json.dumps(json_data))

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        current_user = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'})

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            
            conn = mysql.connect()
            cursor = conn.cursor()

            cursor.execute("""SELECT * from tbl_user WHERE username=%s""", data['username'])
            rv = cursor.fetchall()

            current_user = resultToJson(rv, cursor.description)

        except:
            return jsonify({
                'message': 'Token is invalid!',
                'success': False
            })

        return f(current_user, *args, **kwargs)
    return decorated

class UserListAPI(Resource):
    decorators = [token_required]

    def get(current_user, self):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""SELECT * from tbl_user""")
        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved!"
        )

    def post(current_user, self):
        data = request.get_json()

        conn = mysql.connect()
        cursor = conn.cursor()

        hashed_password = generate_password_hash(data['password'], method='sha256')

        params = {
            'fname': data['fname'],
            'mname': data['mname'],
            'lname': data['lname'],
            'username': data['username'],
            'password': hashed_password,
            'type': data['type']
        }
        query = """INSERT INTO tbl_user (`fname`, `mname`, `lname`, `username`, `password`, `type`) VALUES (%(fname)s, %(mname)s, %(lname)s, %(username)s, %(password)s, %(type)s)"""

        cursor.execute(query, params)
        conn.commit()

        return jsonify(
            success = True,
            message = 'Successfully saved data.'
        )

class UserAPI(Resource):
    decorators = [token_required]
    def get(current_user, self, username):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""SELECT * from tbl_user WHERE username = %s""", (username))
        rv = cursor.fetchall()

        return jsonify(
            data = resultToJson(rv, cursor.description),
            success = True,
            message = 'successfully retreive data.'
        )

    def delete(current_user, self, username):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""DELETE FROM tbl_user WHERE username = %s""", (username))
        conn.commit()

        return jsonify(
            success = True,
            message = 'successfully retreive data.'
        )

class UserSelectedAPI(Resource):
    decorators = [token_required]
    def get(current_user, self, id):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""SELECT * from tbl_user WHERE id = %s""", (id))
        rv = cursor.fetchall()

        return jsonify(
            data = resultToJson(rv, cursor.description),
            success = True,
            message = 'successfully retreive data.'
        )

    def put(current_user, self, id):
        data = request.get_json()

        conn = mysql.connect()
        cursor = conn.cursor()

        hashed_password = ""

        if data['password']:
            hashed_password = generate_password_hash(data['password'], method='sha256')

            query = """UPDATE tbl_user SET
                    username=%(username)s,
                    password=%(password)s,
                    fname=%(fname)s,
                    mname=%(mname)s,
                    lname=%(lname)s,
                    type=%(type)s
                WHERE id = %(id)s"""
        else:
            query = """UPDATE tbl_user SET
                    username=%(username)s,
                    fname=%(fname)s,
                    mname=%(mname)s,
                    lname=%(lname)s,
                    type=%(type)s
                WHERE id = %(id)s"""

        params = {
            'id': id,
            'username': data['username'],
            'password': hashed_password,
            'fname': data['fname'],
            'mname': data['mname'],
            'lname': data['lname'],
            'type': data['type']
        }

        cursor.execute(query, params)
        conn.commit()

        return jsonify(
            success = True,
            message = 'Successfully updated data.'
        )

class UserLogin(Resource):
    def get(self):
        conn = mysql.connect()
        cursor = conn.cursor()

        auth = request.authorization

        if not auth or not auth.username or not auth.password:
            return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})
        
        cursor.execute("""SELECT * from tbl_user WHERE username = %s""", (auth.username))
        rv = cursor.fetchall()

        curr_password = ""
        for data in rv:
            curr_password = data[5]
            
        if curr_password and check_password_hash(curr_password, auth.password):
            token = jwt.encode({
                'username': auth.username,
                'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
            }, app.config['SECRET_KEY'])

            return jsonify(
                success = True,
                token = token.decode('UTF-8')
            )

        # return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})
        return jsonify(
            success = False,
            message = 'Invalid username or password'
        )

class AssetListAPI(Resource):
    decorators = [token_required]

    def get(current_user, self):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""SELECT a.id, a.name, a.description, a.serialnum, a.status, b.username, b.date_requested, b.status AS asset_request_status
            from tbl_asset a
                LEFT JOIN tbl_asset_request b ON a.id = b.asset_id
            GROUP BY b.asset_id, a.id, b.status
        """)
        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved assets!"
        )
    
    def post(current_user, self):
        data = request.get_json()

        conn = mysql.connect()
        cursor = conn.cursor()

        params = {
            'name': data['name'],
            'description': data['description'],
            'serialnum': data['serialnum'],
            'status': 'available'
        }

        query = """INSERT INTO tbl_asset (`name`, `description`, `serialnum`, `status`) VALUES (%(name)s, %(description)s, %(serialnum)s, %(status)s)"""

        cursor.execute(query, params)
        conn.commit()

        return jsonify(
            success = True,
            message = 'Successfully saved data.'
        )

class AssetAPI(Resource):
    decorators = [token_required]

    def get(current_user, self, id):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""SELECT * from tbl_asset WHERE id = %s""", (id))
        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved asset!"
        )

    def put(current_user, self, id):
        data = request.get_json()

        conn = mysql.connect()
        cursor = conn.cursor()

        params = {
            'id': id,
            'name': data['name'],
            'description': data['description'],
            'serialnum': data['serialnum'],
        }

        query = """UPDATE tbl_asset SET name=%(name)s, description=%(description)s, serialnum=%(serialnum)s WHERE id = %(id)s"""

        cursor.execute(query, params)
        conn.commit()

        return jsonify(
            success = True,
            message = 'Successfully updated data.'
        )

    def delete(current_user, self, id):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""DELETE FROM tbl_asset WHERE id = %s""", (id))
        conn.commit()

        return jsonify(
            success = True,
            message = 'successfully deleted data.'
        )

class AssetRequestListAPI(Resource):
    decorators = [token_required]

    def post(current_user, self):
        data = request.get_json()

        conn = mysql.connect()
        cursor = conn.cursor()

        params = {
            'username': data['username'],
            'asset_id': data['asset_id'],
            'status': 'pending',
            'date_requested': datetime.datetime.utcnow()
        }

        queryChecker = """SELECT * FROM tbl_asset_request WHERE username=%(username)s and asset_id=%(asset_id)s and status=%(status)s"""
        
        cursor.execute(queryChecker, params)
        rv = cursor.fetchall()

        print("rv", rv)

        if len(rv) < 1:
            query = """INSERT INTO tbl_asset_request (`username`, `asset_id`, `status`, `date_requested`) VALUES (%(username)s, %(asset_id)s, %(status)s, %(date_requested)s)"""

            cursor.execute(query, params)
            conn.commit()

            return jsonify(
                success = True,
                message = 'You have successfully requested an asset!'
            )

        return jsonify(
            success = False,
            message = 'You already requested this asset!'
        )        
    
    def get(current_user, self):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""
            SELECT a.id, a.username, b.fname, b.mname, b.lname, c.name, c.serialnum, c.status, c.id AS asset_id
            from tbl_asset_request a
                INNER JOIN tbl_user b ON a.username = b.username
                INNER JOIN tbl_asset c ON a.asset_id = c.id
            WHERE a.status = 'pending'
        """)

        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved assets!"
        )

class AssetReturnAPI(Resource):
    decorators = [token_required]

    def put(current_user, self):
        data = request.get_json()

        params = {
            'id': data['id'],
            'username': data['username'],
            'date_requested': data['date_requested'],
            'date_returned': datetime.datetime.utcnow()
        }

        conn = mysql.connect()
        cursor = conn.cursor()
        
        queryAsset = """UPDATE tbl_asset SET status='available' WHERE id=%(id)s"""
        cursor.execute(queryAsset, params)
        conn.commit()

        queryAssetRequest = """DELETE FROM tbl_asset_request WHERE username=%(username)s and status='approved' and asset_id=%(id)s and date_requested=%(date_requested)s"""
        cursor.execute(queryAssetRequest, params)
        conn.commit()
        
        queryAssetLogs = """UPDATE tbl_asset_logs SET date_returned=%(date_returned)s WHERE username=%(username)s and status='approved' and asset_id=%(id)s and date_requested=%(date_requested)s"""
        cursor.execute(queryAssetLogs, params)
        conn.commit()

        return jsonify(
            success = True,
            message = "Successsfully returned asset!"
        )

class AssetRequestAPI(Resource):
    decorators = [token_required]

    def get(current_user, self, id):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""
            SELECT a.id, a.username, b.fname, b.mname, b.lname, c.name, c.serialnum, c.status, a.status AS request_status
            from tbl_asset_request a
                INNER JOIN tbl_user b ON a.username = b.username
                INNER JOIN tbl_asset c ON a.asset_id = c.id
            WHERE a.username = %s
        """, (id))

        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved assets!"
        )

    def put(current_user, self, id):
        data = request.get_json()

        conn = mysql.connect()
        cursor = conn.cursor()

        params = {
            'id': id,
            'status': data['status']
        }

        query = """UPDATE tbl_asset_request SET status=%(status)s WHERE id=%(id)s"""
        cursor.execute(query, params)
        conn.commit()

        if data['status'] == "approved":
            assetParams = {
                'id': data['asset_id']
            }

            assetQuery = """UPDATE tbl_asset SET status='not available' WHERE id=%(id)s"""
            cursor.execute(assetQuery, assetParams)
            conn.commit()

            cursor.execute("""UPDATE tbl_asset_request SET status='denied' WHERE asset_id=%s and id <> %s""", (data['asset_id'], id))
            conn.commit()
        
        logQuery = """INSERT INTO tbl_asset_logs (`username`, `asset_id`, `status`, `date_requested`) SELECT username, asset_id, status, date_requested FROM tbl_asset_request WHERE id = %(id)s"""
        cursor.execute(logQuery, params)
        conn.commit()

        return jsonify(
            success = True,
            message = 'You have successfully ' +  params["status"] + ' a request!'
        )

    def delete(current_user, self, id):
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("""DELETE FROM tbl_asset_request WHERE id = %s""", (id))
        conn.commit()

        return jsonify(
            success = True,
            message = 'You have successfully cancelled your request.'
        )

class AssignedAssetListAPI(Resource):
    decorators = [token_required]

    def get(current_user, self, id):
        conn = mysql.connect()
        cursor = conn.cursor()

        params = {
            'username': id
        }

        query = """
            SELECT *
            from tbl_asset a
                INNER JOIN tbl_asset_request b ON a.id = b.asset_id
            WHERE b.username=%(username)s and b.status='approved'
        """

        cursor.execute(query, params)
        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved assets!"
        )

class HistoryLogsListAPI(Resource):
    decorators = [token_required]
    
    def get(current_user, self):
        conn = mysql.connect()
        cursor = conn.cursor()
        
        cursor.execute("""
            SELECT a.id, a.username, b.fname, b.mname, b.lname, c.name AS asset_name, c.serialnum, a.status, a.date_requested, a.date_returned
            from tbl_asset_logs a
                LEFT JOIN tbl_user b ON a.username = b.username
                LEFT JOIN tbl_asset c ON a.asset_id = c.id
        """)
        
        rv = cursor.fetchall()

        return jsonify(
            success = True,
            data = resultToJson(rv, cursor.description),
            message = "Successfully retrieved assets!"
        )


api.add_resource(UserLogin, '/login')
api.add_resource(UserListAPI, '/user')
api.add_resource(UserSelectedAPI, '/users/<string:id>')
api.add_resource(UserAPI, '/user/<string:username>')
api.add_resource(AssetListAPI, '/asset')
api.add_resource(AssetAPI, '/asset/<string:id>')
api.add_resource(AssetReturnAPI, '/asset/return')
api.add_resource(AssetRequestListAPI, '/asset/request')
api.add_resource(AssetRequestAPI, '/asset/request/<string:id>')
api.add_resource(HistoryLogsListAPI, '/asset/logs')
api.add_resource(AssignedAssetListAPI, '/asset/assigned/<string:id>')

if __name__ == '__main__':
    app.run(debug=True)
